#ifndef __PROGTEST__
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
typedef struct
{
    int m_TotalDays;
    int m_WorkDays;
} TResult;
#endif /* __PROGTEST__ */

// Массив со сдвигами месяцев относительно января для алгоритма Сакамото поиска дня недели
const int G_SHIFT[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
const int SATURDAY = 6;
const int SUNDAY = 0;
const int DAYS_IN_WEEK = 7;
const int DAYS_IN_THE_YEAR = 365;
const int DAYS_IN_THE_LEAP_YEAR = 366;
// Кол-во рабочих дней в году, если год начался в определенный день недели
const int WORK_DAYS_IN_THE_YEAR[] = {252, 252, 253, 253, 252, 254, 254 };
// Кол-во рабочих дней в високосном году, если год начался в определенный день недели
const int WORK_DAYS_IN_THE_LEAP_YEAR[] = {253, 254, 254, 253, 255, 254, 252 };
// Кол-во дней на интервале в 4000 лет
const int DAYS_IN_THE_INTERVAL_4K = 1460969;
//Кол-во рабочих дней на интервале в 4000 лет. В зависимости от того, когда начался интервал, кол-во рабочих дней может отличаться
//Здесь все варианты для каждого дня недели
const int WORK_DAYS_IN_THE_INTERVAL_4K[] = {1012110, 1012119, 1012139, 1012109, 1012139, 1012109, 1012120};

//Вернет кол-во дней в феврале в зависимости от года (проверка на високосный год)
int daysInFebruary(int year){
    if(year % 4 == 0 && (year % 100 != 0 || year % 400 == 0 ) && year % 4000 != 0)
        return 29;
    else
        return 28;
}

//вернет максимальное кол-во дней в месяце
int daysInMonth(int month, int year){
    int daysInMonth[12] = {31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    daysInMonth[1] = daysInFebruary(year);
    return daysInMonth[month - 1];
}

//Обычная проверка даты
bool verifyDate ( int day, int month, int year){
    if(day < 1 || day > daysInMonth(month, year))
        return false;
    if(month < 1 || month > 12)
        return false;
    if(year < 2000 )
        return false;
    return true;
}

//Проверка на праздничный день
bool isCelebrationDay(int day, int month){
    switch (month) {
        case 1:
            return day == 1;
        case 5:
            return day == 1 || day == 8;
        case 7:
            return day == 5 || day == 6;
        case 9:
            return day == 28;
        case 10:
            return day == 28;
        case 11:
            return day == 17;
        case 12:
            return day >= 24 && day <= 26;
        default:
            return false;
    }
}

//Используется алгоритм сакамото для определения дня недели
int dayOfTheWeek(int year, int month, int day){
    year --;
    if ( month >= 3)
        year++;
    return (year + (year/4) - (year/100) + (year/400) - (year/4000) + G_SHIFT[month - 1] + day ) % DAYS_IN_WEEK;
}
// Каждый день недели имеет свой индекс. 0 - ВС, 1 - ПН, 2 - ВТ и т.д.
//Проверка на рабочий день, используя функции выше, а так же учитывая СБ и ВС как выходные
bool isWorkDay (int year, int month, int day )
{
    if(!verifyDate(day, month, year))
        return false;
    if(isCelebrationDay(day, month))
        return false;
    int result = dayOfTheWeek(year, month, day);
    return result > SUNDAY && result < SATURDAY;
}

//Проверка на корректность интервала. Корректность даты1 и даты2, а так же если дата1 > дата2
bool checkInterval(int year1, int month1, int day1, int year2, int month2, int day2){
    if(!verifyDate(day1, month1, year1) || !verifyDate(day2, month2, year2))
        return false;
    else if(year1 > year2)
        return false;
    else if(year1 == year2 && month1 > month2)
        return false;
    else if(year1 == year2 && month1 == month2 && day1 > day2)
        return false;
    else
        return true;
}

// Данная функция считает кол-во дней только в тамках 1 года.
TResult calculateDaysFromTo(int year1, int month1, int day1, int month2, int day2){
    TResult result;
    result.m_TotalDays = result.m_WorkDays = 0;
    int maxDays = daysInMonth(month1, year1);
    while(1){
        if(isWorkDay(year1, month1, day1))
            result.m_WorkDays++;
        result.m_TotalDays++;

        if(month1 == month2 && day1 == day2)
            return result;
        //Если прошли все дни месяца, то переходим на следующий месяц
        if(++day1 > maxDays){
            day1 = 1;
            month1++;
            maxDays = daysInMonth(month1, year1);
        }
    }
}

//Считаем кол-во дней в интервале от year1 до year2. Посчитает кол-во дней от 1.1.year1 до 31.12.year2
//На ввод даем только 1.1.year1 и 1.1.year2, так расчитываем закрытый интервал(2 конца включены)
//Для этого мы и обрезали неполные года ранее, чтобы посчитать полные года
TResult calculateYearsFromTo(int year1, int year2){
    TResult result;
    result.m_TotalDays = result.m_WorkDays = 0;
    if(year1 > year2)
        return result;
    //День недели для 1 января года year1
    int startDay = dayOfTheWeek(year1, 1, 1);
    while (year1 <= year2){
        //Если год високосный, то следующих год начнется на 2 дня позже. Например: 1 января 2004 - четверг, 1 января 2005 - суббота
        if(daysInFebruary(year1) == 29) {
            result.m_TotalDays += DAYS_IN_THE_LEAP_YEAR;
            result.m_WorkDays += WORK_DAYS_IN_THE_LEAP_YEAR[startDay];
            startDay = (startDay + 2) % DAYS_IN_WEEK;
        }
        //Если год не високосный, то следующий год начнется на 1 день позже. Например: 1 января 2005 - суббота, 1 января 2006 - воскресенье
        else {
            result.m_TotalDays += DAYS_IN_THE_YEAR;
            result.m_WorkDays += WORK_DAYS_IN_THE_YEAR[startDay];
            startDay = (startDay + 1) % DAYS_IN_WEEK;
        }
        year1++;
    }
    return result;
}

//Считаем кол-во дней в интервале от year1 до year2. Посчитает кол-во дней от 1.1.year1 до 31.12.year2.
// Известно, что каждый 4000ый год начинается с определенного дня недели на 1 меньше, чем прошлый.
// Например: 1 января 4000 - суббота, 1 января 8000 - пятница, 1 января 12000 - четверг и тд.
// Таким образом, мы можем разбить интервал на 2 части: от 1.1.year1 до 31.12.кратного 4000 и от 1.1.кратного 4000 до 31.12.year2
// Тк здесь есть четкая закономерность, то нет необходимости считать каждые 4000 лет.
TResult calculateIntervals4KFromTo(int year1, int year2){
    TResult result;
    result.m_TotalDays = result.m_WorkDays = 0;
    if(year1 > year2)
        return result;
    //День начала интервала
    int startDay = dayOfTheWeek(year1, 1, 1);
    //День конца интервала. Данный день не будет входить в интервал.
    int endDay = dayOfTheWeek(year2 + 1, 1, 1);

    int cnt = 0;
    //Нам надо найти полный цикл, когда день начала интервала и день конца интервала совпадут.
    //Поэтому опять вручную почитаем кол-во дней в неполном цикле.
    while((startDay % DAYS_IN_WEEK) != endDay){
        result.m_TotalDays += DAYS_IN_THE_INTERVAL_4K;
        result.m_WorkDays += WORK_DAYS_IN_THE_INTERVAL_4K[startDay--];
        cnt++;
    }

    int years = year2 - year1 + 1;
    //Смотрим сколько раз можем поместить 4000 лет в интервал
    int intervals = years / 4000;
    //Считаем кол-во циклов, которые вошли в интервал. Это 28000 лет. Так же учли те 4000 лет, которые не вошли в цикл.
    //Данное число будет кратно 7.
    intervals = (intervals - cnt) / DAYS_IN_WEEK;
    TResult tmpRes;
    tmpRes.m_TotalDays = tmpRes.m_WorkDays = 0;
    //Если есть хотя бы 1 цикл. То считаем сколько всего дней в полном цикле и умножаем на кол-во циклов
    if(intervals){
        for(int i = 0 ; i < DAYS_IN_WEEK ; i++){
            tmpRes.m_TotalDays += DAYS_IN_THE_INTERVAL_4K;
            tmpRes.m_WorkDays += WORK_DAYS_IN_THE_INTERVAL_4K[i];
        }
        tmpRes.m_TotalDays *= intervals;
        tmpRes.m_WorkDays *= intervals;
    }
    //Складываем результаты
    result.m_TotalDays += tmpRes.m_TotalDays;
    result.m_WorkDays += tmpRes.m_WorkDays;
    return result;
}
//Ближайший год, кратный 4000, больше или равный year
int roundUpYear(int year){
    return (year/4000 + 1) * 4000;
}
//Ближайший год, кратный 4000, меньше или равный year
int roundDownYear(int year){
    return (year/4000) * 4000;
}
TResult countDays (int year1, int month1, int day1, int year2, int month2, int day2)
{
    TResult result;
    //Проверим корректность интервала, если нет, то вернем -1, -1
    if(!checkInterval(year1, month1, day1, year2, month2, day2)){
        result.m_TotalDays = result.m_WorkDays = -1;
        return result;
    }
    result.m_TotalDays = result.m_WorkDays = 0;

    //Для интервала в 1 год проще просто проверить каждый день
    if(year1 == year2)
        return calculateDaysFromTo(year1, month1, day1, month2, day2);

    //Если интервал больше, чем 1 год, то:
    //1. Посчитаем дни от даты1 до конца года1
    result = calculateDaysFromTo(year1, month1, day1, 12, 31);
    //2. Посчитаем дни от начала года2 до даты2
    TResult tmpRes = calculateDaysFromTo(year2, 1, 1, month2, day2);
    //Складываем результаты
    result.m_TotalDays += tmpRes.m_TotalDays;
    result.m_WorkDays += tmpRes.m_WorkDays;
    //Если промежуток не очень большой, то пройдем интервал по годам и посчитаем дни
    if(year2/4000 - year1/4000 <= 1){
        tmpRes = calculateYearsFromTo(year1 + 1, year2 - 1);
        result.m_TotalDays += tmpRes.m_TotalDays;
        result.m_WorkDays += tmpRes.m_WorkDays;
        return result;
    }
    //Если промежуток большой, то:
    //1. Посчитаем года от даты1 до первого года, кратного 4000
    tmpRes = calculateYearsFromTo(year1 + 1, roundUpYear(year1 + 1) - 1);
    result.m_TotalDays += tmpRes.m_TotalDays;
    result.m_WorkDays += tmpRes.m_WorkDays;

    //2. Посчитаем года от последнего года, кратного 4000 до даты2
    tmpRes = calculateYearsFromTo(roundDownYear(year2 - 1), year2 - 1);
    result.m_TotalDays += tmpRes.m_TotalDays;
    result.m_WorkDays += tmpRes.m_WorkDays;

    //3. Имеем интрвал, где оба года кратны 4000. Посчитаем дни на этом интервале.
    tmpRes = calculateIntervals4KFromTo(roundUpYear(year1 + 1), roundDownYear(year2 - 1) - 1);
    result.m_TotalDays += tmpRes.m_TotalDays;
    result.m_WorkDays += tmpRes.m_WorkDays;

    return result;
}

#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
{
    TResult r;

    //Небольшой пример по принципу работы алгоритма.
    // Случай 1. 12.4.2021 - 14.7.2021
    // Посчитаем для каждого дня и вернем результат.
    // Случай 2. 12.4.2021 - 14.7.2022
    // Посчитаем 12.4.2021 - 31.12.2021 и 1.1.2022 - 14.7.2022
    // Случай 3. 12.4.2021 - 14.7.2042
    // Посчитаем 12.4.2021 - 31.12.2021, 1.1.2022 - 31.12.2041, 1.1.2042 - 14.7.2042
    // Случай 4. 12.4.3499 - 14.7.5932
    // Посчитаем 12.4.3499 - 31.12.3499, 1.1.3500 - 31.12.5931, 1.1.5932 - 14.7.5932
    // Случай 5. 12.4.3443 - 14.7.28114
    // Посчитаем 12.4.3443 - 31.12.3443, 1.1.28014 - 14.7.28014, 1.1.3444 - 31.12.3999, 1.1.28000 - 31.12.28013, 1.1.4000 - 31.12.27999.
    assert ( isWorkDay ( 2023, 10, 10 ) );

    assert ( ! isWorkDay ( 2023, 11, 11 ) );

    assert ( ! isWorkDay ( 2023, 11, 17 ) );

    assert ( ! isWorkDay ( 2023, 11, 31 ) );

    assert ( ! isWorkDay ( 2023,  2, 29 ) );

    assert ( ! isWorkDay ( 2004,  2, 29 ) );

    assert ( isWorkDay ( 2008,  2, 29 ) );

    assert ( ! isWorkDay ( 2001,  2, 29 ) );

    assert ( ! isWorkDay ( 1996,  1,  2 ) );

    r = countDays ( 2023, 11,  1,
                    2023, 11, 30 );
    assert ( r . m_TotalDays == 30 );
    assert ( r . m_WorkDays == 21 );

    r = countDays ( 2023, 11,  1,
                    2023, 11, 17 );
    assert ( r . m_TotalDays == 17 );
    assert ( r . m_WorkDays == 12 );

    r = countDays ( 2023, 11,  1,
                    2023, 11,  1 );
    assert ( r . m_TotalDays == 1 );
    assert ( r . m_WorkDays == 1 );

    r = countDays ( 2023, 11, 17,
                    2023, 11, 17 );
    assert ( r . m_TotalDays == 1 );
    assert ( r . m_WorkDays == 0 );

    r = countDays ( 2023,  1,  1,
                    2023, 12, 31 );
    assert ( r . m_TotalDays == 365 );
    assert ( r . m_WorkDays == 252 );

    r = countDays ( 2024,  1,  1,
                    2024, 12, 31 );
    assert ( r . m_TotalDays == 366 );
    assert ( r . m_WorkDays == 254 );

    r = countDays ( 2000,  1,  1,
                    2023, 12, 31 );
    assert ( r . m_TotalDays == 8766 );
    assert ( r . m_WorkDays == 6072 );

    r = countDays ( 2001,  2,  3,
                    2023,  7, 18 );
    assert ( r . m_TotalDays == 8201 );
    assert ( r . m_WorkDays == 5682 );

    r = countDays ( 2021,  3, 31,
                    2023, 11, 12 );
    assert ( r . m_TotalDays == 957 );
    assert ( r . m_WorkDays == 666 );

    r = countDays ( 2001,  1,  1,
                    2000,  1,  1 );
    assert ( r . m_TotalDays == -1 );
    assert ( r . m_WorkDays == -1 );

    r = countDays ( 2001,  1,  1,
                    2023,  2, 29 );
    assert ( r . m_TotalDays == -1 );
    assert ( r . m_WorkDays == -1 );

    r = countDays ( 4004, 1,  1,4004, 12, 31 );
    assert ( r . m_TotalDays == 366 );
    assert ( r . m_WorkDays == 253 );

    r = countDays ( 8004, 1,  1,8004,  12, 31 );
    assert ( r . m_TotalDays == 366 );
    assert ( r . m_WorkDays == 254 );

    r = countDays ( 12004, 1,  1,12004, 12, 31 );
    assert ( r . m_TotalDays == 366 );
    assert ( r . m_WorkDays == 254 );

    r = countDays ( 16004, 1,  1,16004, 12, 31 );
    assert ( r . m_TotalDays == 366 );
    assert ( r . m_WorkDays == 253 );

    r = countDays ( 20004, 1,  1,20004, 12, 31 );
    assert ( r . m_TotalDays == 366 );
    assert ( r . m_WorkDays == 252 );

    r = countDays ( 24004, 1,  1,24004, 12, 31 );
    assert ( r . m_TotalDays == 366 );
    assert ( r . m_WorkDays == 254 );

    r = countDays ( 28004, 1,  1,28004, 12, 31 );
    assert ( r . m_TotalDays == 366 );
    assert ( r . m_WorkDays == 255 );

    r = countDays ( 4000, 1,  1,4000, 12, 31 );
    assert ( r . m_TotalDays == 365 );
    assert ( r . m_WorkDays == 254 );

    r = countDays ( 8000, 1,  1,8000,  12, 31 );
    assert ( r . m_TotalDays == 365 );
    assert ( r . m_WorkDays == 254 );

    r = countDays ( 12000, 1,  1,12000, 12, 31 );
    assert ( r . m_TotalDays == 365 );
    assert ( r . m_WorkDays == 252 );

    r = countDays ( 16000, 1,  1,16000, 12, 31 );
    assert ( r . m_TotalDays == 365 );
    assert ( r . m_WorkDays == 253 );

    r = countDays ( 20000, 1,  1,20000, 12, 31 );
    assert ( r . m_TotalDays == 365 );
    assert ( r . m_WorkDays == 253 );

    r = countDays ( 24000, 1,  1,24000, 12, 31 );
    assert ( r . m_TotalDays == 365 );
    assert ( r . m_WorkDays == 252 );

    r = countDays ( 28000, 1,  1,28000, 12, 31 );
    assert ( r . m_TotalDays == 365 );
    assert ( r . m_WorkDays == 252 );

    r = countDays ( 4000, 1,  1,7999, 12, 31 );
    assert ( r . m_TotalDays == 1460969 );
    assert ( r . m_WorkDays == 1012120 );

    r = countDays ( 8000, 1,  1,11999,  12, 31 );
    assert ( r . m_TotalDays == 1460969 );
    assert ( r . m_WorkDays == 1012109 );

    r = countDays ( 12000, 1,  1,15999, 12, 31 );
    assert ( r . m_TotalDays == 1460969 );
    assert ( r . m_WorkDays == 1012139 );


    r = countDays ( 16000, 1,  1,19999, 12, 31 );
    assert ( r . m_TotalDays == 1460969 );
    assert ( r . m_WorkDays == 1012109 );


    r = countDays ( 20000, 1,  1,23999, 12, 31 );
    assert ( r . m_TotalDays == 1460969 );
    assert ( r . m_WorkDays == 1012139 );


    r = countDays ( 24000, 1,  1,27999, 12, 31 );
    assert ( r . m_TotalDays == 1460969 );
    assert ( r . m_WorkDays == 1012119 );


    r = countDays ( 28000, 1,  1,31999, 12, 31 );
    assert ( r . m_TotalDays == 1460969 );
    assert ( r . m_WorkDays == 1012110 );

    r = countDays (2005, 3, 15, 2018, 7, 22);
    assert ( r . m_TotalDays == 4878 );
    assert ( r . m_WorkDays == 3378 );

    r = countDays (2022, 11, 8, 2035, 1, 17);
    assert ( r . m_TotalDays == 4454 );
    assert ( r . m_WorkDays == 3085 );


    r = countDays (25000, 6, 12, 30123, 9, 30);
    assert ( r . m_TotalDays == 1871247 );
    assert ( r . m_WorkDays == 1296347 );


    r = countDays (4567, 2, 28, 4567, 12, 31);
    assert ( r . m_TotalDays == 307 );
    assert ( r . m_WorkDays == 212 );

    r = countDays (18000, 1, 1, 18000, 12, 10);
    assert ( r . m_TotalDays == 345 );
    assert ( r . m_WorkDays == 242 );

    r = countDays (4568, 1, 1, 7999, 12, 31);
    assert ( r . m_TotalDays == 1253512 );
    assert ( r . m_WorkDays == 868401 );

    r = countDays (16000, 1, 1, 17999, 12, 31);
    assert ( r . m_TotalDays == 730484 );
    assert ( r . m_WorkDays == 506054 );

    r = countDays (8000, 1, 1, 15999, 12, 31);
    assert ( r . m_TotalDays == 2921938 );
    assert ( r . m_WorkDays == 2024248 );

    r = countDays (4567, 2, 28, 18000, 12, 10);
    assert ( r . m_TotalDays == 4906586 );
    assert ( r . m_WorkDays == 3399157 );

    r = countDays (2001, 9, 5, 2023, 4, 18);
    assert ( r . m_TotalDays == 7896 );
    assert ( r . m_WorkDays == 5473 );

    r = countDays (12000, 1, 1, 18000, 6, 30);
    assert ( r . m_TotalDays == 2191635 );
    assert ( r . m_WorkDays == 1518320 );

    r = countDays (30500, 11, 20, 40000, 2, 5);
    assert ( r . m_TotalDays == 3469514 );
    assert ( r . m_WorkDays == 2403570 );

    r = countDays (2200, 4, 7, 7890, 12, 25);
    assert ( r . m_TotalDays == 2078492 );
    assert ( r . m_WorkDays == 1439916 );

    r = countDays (15000, 8, 3, 30000, 5, 14);
    assert ( r . m_TotalDays == 5478554 );
    assert ( r . m_WorkDays == 3795400 );

    r = countDays (4500, 10, 18, 17000, 2, 9);
    assert ( r . m_TotalDays == 4565278 );
    assert ( r . m_WorkDays == 3162707 );

    r = countDays (2000, 8, 12, 40321, 4, 26);
    assert ( r . m_TotalDays == 13996340 );
    assert ( r . m_WorkDays == 9696280 );

    r = countDays (3783, 8, 12, 7654, 4, 26);
    assert ( r . m_TotalDays == 1413746 );
    assert ( r . m_WorkDays == 979406 );

    r = countDays (4000, 8, 12, 7654, 4, 26);
    assert ( r . m_TotalDays == 1334489 );
    assert ( r . m_WorkDays == 924498 );

    r = countDays (4000, 1, 1, 8000, 1, 1);
    assert ( r . m_TotalDays == 1460970 );
    assert ( r . m_WorkDays == 1012120 );

    r = countDays (4000, 8, 12, 12000, 4, 26);
    assert ( r . m_TotalDays == 2921831 );
    assert ( r . m_WorkDays == 2024153 );

    r = countDays (4000, 1, 1, 12000, 1, 1);
    assert ( r . m_TotalDays == 2921939 );
    assert ( r . m_WorkDays == 2024229 );

    r = countDays (6532, 12, 21, 43513, 2, 12);
    assert ( r . m_TotalDays == 13506712 );
    assert ( r . m_WorkDays == 9357096 );

    r = countDays (2023, 12, 21, 2025, 2, 12);
    assert ( r . m_TotalDays == 420 );
    assert ( r . m_WorkDays == 289 );

    r = countDays (2023, 12, 21, 2024, 2, 12);
    assert ( r . m_TotalDays == 54 );
    assert ( r . m_WorkDays == 35 );

    return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */
