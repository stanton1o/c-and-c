#include <stdio.h>
#include <stdbool.h>
#include <memory.h>

#define MAX_GEARS 50

typedef struct {
    long long numerator;
    long long denominator;
} Pair;

typedef struct {
    long long numerator;
    long long denominator;
    double ratio;
    double ratioWithNeed;
    Pair path[MAX_GEARS];
    int pathSize;
} Gear;

long long gcd(long long a, long long b) {
    while (b != 0) {
        long long t = b;
        b = a % b;
        a = t;
    }
    return a;
}

//Тк все соотношения больше 1(так мы переворачивали дроь) то наилучшее соотношение будет самым маленьким, тк будет ближе всего к 1
Gear min(Gear a, Gear b) {
    if (a.ratioWithNeed < b.ratioWithNeed) {
        return a;
    }
    return b;
}

//Тк в Си нецелые числа содержат неточности, то для сохранения точности дробь переворачивается.
double calculateRatio(double numerator, double denominator) {
    double ratio = numerator / denominator;
    if (ratio < 1) {
        return denominator / numerator;
    }
    return ratio;
}

Gear calculate(double needRatio, Gear gears[], int currentGear, Gear currentRatio, int pathIndex) {
    if (currentGear < 0) {
        return currentRatio;
    }

    Gear bestRatio = currentRatio;
    bestRatio.ratioWithNeed = calculateRatio(bestRatio.ratio, needRatio);
    //Берем шестерню
    Gear gear = gears[currentGear];

    // Проверяем num/denom
    Gear ratioNumDenom = {gear.numerator * currentRatio.numerator, gear.denominator * currentRatio.denominator};
    memcpy(ratioNumDenom.path, currentRatio.path, sizeof(Pair) * MAX_GEARS);
    ratioNumDenom.path[pathIndex].numerator = gears[currentGear].numerator;
    ratioNumDenom.path[pathIndex].denominator = gears[currentGear].denominator;
    ratioNumDenom.pathSize = pathIndex;
    ratioNumDenom.ratio = (double) ratioNumDenom.numerator / (double) ratioNumDenom.denominator;
    ratioNumDenom = calculate(needRatio, gears, currentGear - 1, ratioNumDenom, pathIndex + 1);
    ratioNumDenom.ratioWithNeed = calculateRatio(ratioNumDenom.ratio, needRatio);
    bestRatio = min(bestRatio, ratioNumDenom);


    // Проверяем denom/num
    Gear ratioDenomNum = {gear.denominator * currentRatio.numerator, gear.numerator * currentRatio.denominator};
    memcpy(ratioDenomNum.path, currentRatio.path, sizeof(Pair) * MAX_GEARS);
    ratioDenomNum.path[pathIndex].numerator = gears[currentGear].denominator;
    ratioDenomNum.path[pathIndex].denominator = gears[currentGear].numerator;
    ratioDenomNum.pathSize = pathIndex;
    ratioDenomNum.ratio = (double) ratioDenomNum.numerator / (double) ratioDenomNum.denominator;
    ratioDenomNum = calculate(needRatio, gears, currentGear - 1, ratioDenomNum, pathIndex + 1);
    ratioDenomNum.ratioWithNeed = calculateRatio(ratioDenomNum.ratio, needRatio);
    bestRatio = min(bestRatio, ratioDenomNum);

    // не берем шестерню
    Gear ratioWithoutGear = calculate(needRatio, gears, currentGear - 1, currentRatio, pathIndex);
    ratioWithoutGear.ratioWithNeed = calculateRatio(ratioWithoutGear.ratio, needRatio);
    bestRatio = min(bestRatio, ratioWithoutGear);

    return bestRatio;
}

void callCalculate(long long numerator, long long denominator, Gear gears[], int gearsCount){
    Gear startGear = {1, 1, 1.0, 0};

    startGear = calculate((double) numerator / (double) denominator, gears, gearsCount - 1, startGear, 0);
    long long divider = gcd(startGear.numerator, startGear.denominator);

    if(startGear.numerator == 1 && startGear.denominator == 1){
        printf("%lld:%lld\n", startGear.numerator / divider, startGear.denominator / divider);
        return;
    }

    printf("%lld:%lld = ", startGear.numerator / divider, startGear.denominator / divider);
    for (int i = 0; i < startGear.pathSize; i++) {
        printf("[%lld:%lld] * ", startGear.path[i].numerator, startGear.path[i].denominator);
    }
    printf("[%lld:%lld]\n", startGear.path[startGear.pathSize].numerator, startGear.path[startGear.pathSize].denominator);
}

int main() {
    Gear gears[MAX_GEARS];
    bool canBeAdded = true;
    int gearsCount = 0;
    long long numerator, denominator;
    char operation, delimiter;

    printf("Prevody:\n");

    while (scanf(" %c %lld %c %lld", &operation, &numerator, &delimiter, &denominator) == 4) {
        if (operation == '+') {
            if (numerator < 10 || numerator > 1000 || denominator < 10 || denominator > 1000 || !canBeAdded || delimiter != ':') {
                printf("Nespravny vstup.\n");
                return 1;
            }
            gears[gearsCount].numerator = numerator;
            gears[gearsCount].denominator = denominator;
            gearsCount++;
        } else if (operation == '?') {
            canBeAdded = false;
            if (numerator <= 0 || denominator <= 0 || delimiter != ':') {
                printf("Nespravny vstup.\n");
                return 1;
            }
            callCalculate(numerator, denominator, gears, gearsCount);
        } else {
            printf("Nespravny vstup.\n");
            return 1;
        }
    }
    if(!feof(stdin)){
        printf("Nespravny vstup.\n");
        return 1;
    }

    return 0;
}