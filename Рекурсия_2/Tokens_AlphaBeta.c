#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
const int MAX_TOKENS = 100;
const int MAX_PLAYERS = 2;

enum choiceType{
    FIRST,
    LAST,
    TWO_FIRST,
    TWO_LAST,
    FIRST_LAST
};
enum playerType{
    PLAYER1,
    PLAYER2
};
typedef struct{
    int points;
    bool canTake2;
    int playerType;
    bool isMaxPlayer;
}Player;

typedef struct{
    int tokens[100];
    int numTokens;
}Token;

typedef struct {
    int alpha;
    int beta;
}AlphaBeta;

Player initializePlayer(int playerType){
    Player player;
    player.points = 0;
    player.canTake2 = true;
    player.playerType = playerType;
    player.isMaxPlayer = playerType == PLAYER1;
    return player;
}
bool writeTokens(Token *t_tokens, int size){
    int i = 0, token = 0;
    printf("Zetony:\n");
    while(scanf(" %d", &token) == 1){
        if(i == size){
            return false;
        }
        t_tokens->tokens[i++] = token;
        t_tokens->numTokens = i ;
    }
    return feof(stdin) && t_tokens->numTokens > 0;
}

int changePlayer(int currentPlayer){
    return currentPlayer == PLAYER1 ? PLAYER2 : PLAYER1;
}

int makeChoice(Token *t_tokens, Player* players, int currentPlayer, int startToken, int endToken, AlphaBeta alphaBeta, int* lastChoice){

    int choice;
    int maxPoints;
    if(players[currentPlayer].isMaxPlayer){
        maxPoints = INT_MIN;
    } else {
        maxPoints = INT_MAX;
    }
    if(startToken > endToken){
        if(players[currentPlayer].isMaxPlayer){
            return players[currentPlayer].points;
        } else {
            return players[changePlayer(currentPlayer)].points;
        }
    }

    //Player takes first token
    Player tmpPlayer = players[currentPlayer];
    players[currentPlayer].canTake2 = true;
    players[currentPlayer].points += t_tokens->tokens[startToken];
    int result = makeChoice(t_tokens, players, changePlayer(currentPlayer), startToken + 1, endToken, alphaBeta, lastChoice);
    players[currentPlayer] = tmpPlayer;

    if(players[currentPlayer].isMaxPlayer){
        if(result > maxPoints){
            maxPoints = result;
            choice = FIRST;
            alphaBeta.alpha = maxPoints;
        }
    } else {
        if(result < maxPoints){
            maxPoints = result;
            choice = FIRST;
            alphaBeta.beta = maxPoints;
        }
    }

    if(startToken == endToken || alphaBeta.beta <= alphaBeta.alpha){
        *lastChoice = choice;
        return maxPoints;
    }
    //Player takes last token
    tmpPlayer = players[currentPlayer];
    players[currentPlayer].canTake2 = true;
    players[currentPlayer].points += t_tokens->tokens[endToken];
    result = makeChoice(t_tokens, players, changePlayer(currentPlayer), startToken, endToken - 1, alphaBeta, lastChoice);
    players[currentPlayer] = tmpPlayer;

    if(players[currentPlayer].isMaxPlayer){
        if(result > maxPoints){
            maxPoints = result;
            choice = LAST;
            alphaBeta.alpha = maxPoints;
        }
    } else {
        if(result < maxPoints){
            maxPoints = result;
            choice = LAST;
            alphaBeta.beta = maxPoints;
        }
    }

    if(!players[currentPlayer].canTake2 || startToken == endToken || alphaBeta.beta <= alphaBeta.alpha){
        *lastChoice = choice;
        return maxPoints;
    }

    //Player takes two first tokens
    tmpPlayer = players[currentPlayer];
    players[currentPlayer].canTake2 = false;
    players[currentPlayer].points += t_tokens->tokens[startToken] + t_tokens->tokens[startToken + 1];
    result = makeChoice(t_tokens, players, changePlayer(currentPlayer), startToken + 2, endToken, alphaBeta, lastChoice);
    players[currentPlayer] = tmpPlayer;

    if(players[currentPlayer].isMaxPlayer){
        if(result > maxPoints){
            maxPoints = result;
            choice = TWO_FIRST;
            alphaBeta.alpha = maxPoints;
        }
    } else {
        if(result < maxPoints){
            maxPoints = result;
            choice = TWO_FIRST;
            alphaBeta.beta = maxPoints;
        }
    }

    if(startToken + 1 == endToken || alphaBeta.beta <= alphaBeta.alpha){
        *lastChoice = choice;
        return maxPoints;
    }
    //Player takes two last tokens
    tmpPlayer = players[currentPlayer];
    players[currentPlayer].canTake2 = false;
    players[currentPlayer].points += t_tokens->tokens[endToken] + t_tokens->tokens[endToken - 1];
    result = makeChoice(t_tokens, players, changePlayer(currentPlayer), startToken, endToken - 2, alphaBeta, lastChoice);
    players[currentPlayer] = tmpPlayer;

    if(players[currentPlayer].isMaxPlayer){
        if(result > maxPoints){
            maxPoints = result;
            choice = TWO_LAST;
            alphaBeta.alpha = maxPoints;
        }
    } else {
        if(result < maxPoints){
            maxPoints = result;
            choice = TWO_LAST;
            alphaBeta.beta = maxPoints;
        }
    }
    if(alphaBeta.beta <= alphaBeta.alpha){
        *lastChoice = choice;
        return maxPoints;
    }
    //Player takes first and last token
    tmpPlayer = players[currentPlayer];
    players[currentPlayer].canTake2 = false;
    players[currentPlayer].points += t_tokens->tokens[startToken] + t_tokens->tokens[endToken];
    result = makeChoice(t_tokens, players, changePlayer(currentPlayer), startToken + 1, endToken - 1, alphaBeta, lastChoice);
    players[currentPlayer] = tmpPlayer;

    if(players[currentPlayer].isMaxPlayer){
        if(result > maxPoints){
            maxPoints = result;
            choice = FIRST_LAST;
        }
    } else {
        if(result < maxPoints){
            maxPoints = result;
            choice = FIRST_LAST;
        }
    }
    *lastChoice = choice;
    return maxPoints;
}

void game(Token *t_tokens, Player* players, int currentPlayer, int startToken, int endToken){

    int choice;
    AlphaBeta alphaBeta;
    alphaBeta.alpha = INT_MIN;
    alphaBeta.beta = INT_MAX;
    while (startToken <= endToken){

        makeChoice(t_tokens, players, currentPlayer, startToken, endToken, alphaBeta, &choice);
        switch (choice) {
            case FIRST:
                players[currentPlayer].canTake2 = true;
                players[currentPlayer].points += t_tokens->tokens[startToken];
                printf("%c [%d]: %d\n", currentPlayer + 'A', startToken, t_tokens->tokens[startToken]);
                startToken++;
                break;
            case LAST:
                players[currentPlayer].canTake2 = true;
                players[currentPlayer].points += t_tokens->tokens[endToken];
                printf("%c [%d]: %d\n", currentPlayer + 'A', endToken, t_tokens->tokens[endToken]);
                endToken--;
                break;
            case TWO_FIRST:
                players[currentPlayer].canTake2 = false;
                players[currentPlayer].points += t_tokens->tokens[startToken] + t_tokens->tokens[startToken + 1];
                printf("%c [%d], [%d]: %d + %d\n", currentPlayer + 'A', startToken, startToken + 1, t_tokens->tokens[startToken], t_tokens->tokens[startToken + 1]);
                startToken += 2;
                break;
            case TWO_LAST:
                players[currentPlayer].canTake2 = false;
                players[currentPlayer].points += t_tokens->tokens[endToken] + t_tokens->tokens[endToken - 1];
                printf("%c [%d], [%d]: %d + %d\n", currentPlayer + 'A', endToken, endToken - 1, t_tokens->tokens[endToken], t_tokens->tokens[endToken - 1]);
                endToken -= 2;
                break;
            case FIRST_LAST:
                players[currentPlayer].canTake2 = false;
                players[currentPlayer].points += t_tokens->tokens[startToken] + t_tokens->tokens[endToken];
                printf("%c [%d], [%d]: %d + %d\n", currentPlayer + 'A', startToken, endToken, t_tokens->tokens[startToken], t_tokens->tokens[endToken]);
                startToken++;
                endToken--;
                break;
        }
        currentPlayer = changePlayer(currentPlayer);

    }
}
int main(){
    Token t_tokens;

    if(!writeTokens(&t_tokens, MAX_TOKENS)){
        printf("Nespravny vstup.\n");
        return 1;
    }
    Player players[MAX_PLAYERS];
    players[PLAYER1] = initializePlayer(PLAYER1);
    players[PLAYER2] = initializePlayer(PLAYER2);
    game(&t_tokens, players, PLAYER1, 0, t_tokens.numTokens - 1);

    printf("A: %d, ", players[PLAYER1].points);
    printf("B: %d\n", players[PLAYER2].points);
    return 0;
}
