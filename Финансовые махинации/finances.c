#include <stdio.h>

const int MAX = 250000;

int findMax(const int finances[], int size){
    int max = 0;
    for(int i = 0 ; i < size ; i++){

        for(int j = i + max ; j < size ; j ++){
            if(finances[j] >= finances[i]){
                max = j - i > max ? j - i : max;
            }
        }
    }
    return max;
}

void calculateIntervals(const int finances[], int max, int size){

    if(max == 0){
        printf("Nelze najit.\n");
        return;
    }
    int len = size - max - 1;
    int cnt = 0;
    for(int i = 0 ; i <= len; i++){
        if(finances[i] <= finances[i+max]){
            cnt++;
            printf("%d: %d - %d\n", max + 1, i, i+max);
        }
    }
    printf("Moznosti: %d\n", cnt);
}
int main(){
    int finances[MAX + 1];
    int cnt = 0;

    printf("Hodnoty:\n");
    while(scanf(" %d", &finances[cnt]) == 1){

        if(cnt++ >= MAX){
            printf("Nespravny vstup.\n");
            return 1;
        }
    }
    if(cnt < 2 || !feof(stdin)){
        printf("Nespravny vstup.\n");
        return 1;
    }

    calculateIntervals(finances, findMax(finances, cnt), cnt);
    return 0;
}