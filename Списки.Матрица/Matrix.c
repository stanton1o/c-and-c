
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct TCell {
    struct TCell *m_Right;
    struct TCell *m_Down;
    int m_Row;
    int m_Col;
    int m_Data;
} TCELL;

typedef struct TRowCol {
    struct TRowCol *m_Next;
    TCELL *m_Cells;
    int m_Idx;
} TROWCOL;

typedef struct TSparseMatrix {
    TROWCOL *m_Rows;
    TROWCOL *m_Cols;
} TSPARSEMATRIX;



void initMatrix(TSPARSEMATRIX *m) {
    m->m_Rows = m->m_Cols = nullptr;
}

TCELL* findTopCell(TCELL* cell, int rowIdx, TCELL* parent){
    if(!cell || cell->m_Row > rowIdx) return parent;
    if(cell->m_Row == rowIdx) return cell;
    return findTopCell(cell->m_Down, rowIdx, cell);
}

TCELL* findLeftCell(TCELL* cell, int colIdx, TCELL* parent){
    if(!cell || cell->m_Col > colIdx) return parent;
    if(cell->m_Col == colIdx) return cell;
    return findLeftCell(cell->m_Right, colIdx, cell);
}

TROWCOL* findRowCol(TROWCOL* rowCol, int idx, TROWCOL* parent){
    if(!rowCol || rowCol->m_Idx > idx) return parent;
    if(rowCol->m_Idx == idx) return rowCol;
    return findRowCol(rowCol->m_Next, idx, rowCol);
}

TROWCOL * insertRowCol(TROWCOL * rowCol, int rowColIdx, TROWCOL ** first) {
    if(!rowCol){
        TROWCOL * tmp = (*first);
        (*first) = (TROWCOL *) malloc(sizeof(TROWCOL));
        (*first)->m_Next = tmp;
        (*first)->m_Idx = rowColIdx;
        (*first)->m_Cells = nullptr;
        return (*first);
    }
    if (rowCol->m_Idx == rowColIdx) return rowCol;
    TROWCOL * newRowCol = (TROWCOL *) malloc(sizeof(TROWCOL));
    newRowCol->m_Idx = rowColIdx;
    newRowCol->m_Cells = nullptr;
    newRowCol->m_Next = rowCol->m_Next;
    rowCol->m_Next = newRowCol;
    return newRowCol;
}

void addSetCell(TSPARSEMATRIX *m, int rowIdx, int colIdx, int data) {
    TROWCOL * col = findRowCol(m->m_Cols, colIdx, nullptr);
    if(!col || col->m_Idx != colIdx) {
        col = insertRowCol(col, colIdx, &m->m_Cols);
    }
    TROWCOL * row = findRowCol(m->m_Rows, rowIdx, nullptr);
    if(!row || row->m_Idx != rowIdx) {
        row = insertRowCol(row, rowIdx, &m->m_Rows);
    }

    TCELL* topCell = findTopCell(col->m_Cells, rowIdx, nullptr);

    if(!topCell) {
        TCELL* tmp = col->m_Cells;
        col->m_Cells = (TCELL*)malloc(sizeof(TCELL));
        col->m_Cells->m_Down = tmp;
        col->m_Cells->m_Row = rowIdx;
        col->m_Cells->m_Col = colIdx;
        col->m_Cells->m_Data = data;
        topCell = col->m_Cells;
    } else if (topCell->m_Row != rowIdx){
        TCELL* tmp = topCell->m_Down;
        topCell->m_Down = (TCELL*)malloc(sizeof(TCELL));
        topCell->m_Down->m_Down = tmp;
        topCell->m_Down->m_Row = rowIdx;
        topCell->m_Down->m_Col = colIdx;
        topCell->m_Down->m_Data = data;
        topCell = topCell->m_Down;
    } else{
        topCell->m_Data = data;
        return;
    }
    TCELL* leftCell = findLeftCell(row->m_Cells, colIdx, nullptr);

    if(!leftCell) {
        TCELL* tmp = row->m_Cells;
        row->m_Cells = topCell;
        topCell->m_Right = tmp;
    } else{
        TCELL* tmp = leftCell->m_Right;
        leftCell->m_Right = topCell;
        topCell->m_Right = tmp;
    }

}

bool removeCell(TSPARSEMATRIX *m, int rowIdx, int colIdx){
    TROWCOL* row = findRowCol(m->m_Rows, rowIdx, nullptr);
    if(!row || row->m_Idx != rowIdx) return false;
    TROWCOL* col = findRowCol(m->m_Cols, colIdx, nullptr);
    if(!col || col->m_Idx != colIdx) return false;
    TCELL* delCell = findTopCell(col->m_Cells, rowIdx, nullptr);
    if(!delCell || delCell->m_Row != rowIdx) return false;
    TCELL* parentTopCell = findTopCell(col->m_Cells, rowIdx - 1, nullptr);
    if(!parentTopCell) {
        col->m_Cells = delCell->m_Down;
        if(!col->m_Cells) {
            TROWCOL* parentCol = findRowCol(m->m_Cols, colIdx - 1, nullptr);
            if(!parentCol) {
                m->m_Cols = col->m_Next;
            } else {
                parentCol->m_Next = col->m_Next;
            }
            free(col);
        }
    } else {
        parentTopCell->m_Down = delCell->m_Down;
    }
    TCELL* parentLeftCell = findLeftCell(row->m_Cells, colIdx - 1, nullptr);
    if(!parentLeftCell) {
        row->m_Cells = delCell->m_Right;
        if(!row->m_Cells) {
            TROWCOL* parentRow = findRowCol(m->m_Rows, rowIdx - 1, nullptr);
            if(!parentRow) {
                m->m_Rows = row->m_Next;
            } else {
                parentRow->m_Next = row->m_Next;
            }
            free(row);
        }
    } else {
        parentLeftCell->m_Right = delCell->m_Right;
    }
    free(delCell);

    return true;
}

void freeMatrix(TSPARSEMATRIX *m) {
    TROWCOL * freeCol = m->m_Cols, *freeRow = m->m_Rows;

    initMatrix(m);
    for(TROWCOL* row = freeRow; row; ) {
        TROWCOL* tmp = row->m_Next;
        free(row);
        row = tmp;
    }
    for(TROWCOL* col = freeCol; col; ) {
        for(TCELL* cell = col->m_Cells; cell;){
            TCELL* tmp = cell->m_Down;
            free(cell);
            cell = tmp;
        }
        TROWCOL* tmp = col->m_Next;
        free(col);
        col = tmp;
    }
}



int main(int argc, char *argv[]){
    
    return 0;
}

