#ifndef __PROGTEST__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define nullptr NULL
typedef struct TCriminal {
    struct TCriminal *m_Next;
    char *m_Name;
    struct TCriminal **m_Contacts;
    size_t m_Cnt;
    size_t m_Capacity;
} TCRIMINAL;

#endif /* __PROGTEST__ */

TCRIMINAL *createRecord(const char *name,
                        TCRIMINAL *next) {
    TCRIMINAL *newCriminal = (TCRIMINAL *) malloc(sizeof(TCRIMINAL));
    newCriminal->m_Name = strdup(name);
    newCriminal->m_Next = next;
    newCriminal->m_Cnt = 0;
    newCriminal->m_Capacity = 0;
    newCriminal->m_Contacts = nullptr;
    return newCriminal;
}

TCRIMINAL *createCopyOfNode(TCRIMINAL* src){
    TCRIMINAL *newCriminal = (TCRIMINAL *) malloc(sizeof(TCRIMINAL));
    newCriminal->m_Name = strdup(src->m_Name);
    newCriminal->m_Next = src->m_Next;
    newCriminal->m_Cnt = src->m_Cnt;
    newCriminal->m_Capacity = src->m_Capacity;
    newCriminal->m_Contacts = nullptr;
    return newCriminal;
}

//Split list into two lists. First list contains every odd element, second contains every even element
void splitList(TCRIMINAL *src, TCRIMINAL **first, TCRIMINAL **second) {
    if(!src) return;
    *first = src;
    *second = src->m_Next;
    TCRIMINAL *tmp = *second;
    while(tmp && tmp->m_Next) {
        src->m_Next = tmp->m_Next;
        src = src->m_Next;
        tmp->m_Next = src->m_Next;
        tmp = tmp->m_Next;
    }
    src->m_Next = nullptr;
}

TCRIMINAL *cloneList(TCRIMINAL *src) {
    if(!src) return nullptr;
    TCRIMINAL *newList = src;

 // создание копии узла и вставки его на следующее место
    while(newList) {
        TCRIMINAL* tmp = createCopyOfNode(newList);
        newList->m_Next = tmp;
        newList = tmp->m_Next;
    }
    //копирование случайный указателей для копии
    newList = src;
    while(newList) {
        if(newList->m_Cnt > 0) {
            newList->m_Next->m_Contacts = (TCRIMINAL **) malloc(newList->m_Capacity * sizeof(TCRIMINAL *));
            for(size_t i = 0; i < newList->m_Cnt; i++) {
                newList->m_Next->m_Contacts[i] = newList->m_Contacts[i]->m_Next;
            }
        }
        newList = newList->m_Next->m_Next;
    }
    newList = src;
    TCRIMINAL *copy;
    splitList(newList, &src, &copy);
    return copy;
}

//добавить случайный указатель
void addContact(TCRIMINAL *dst,
                TCRIMINAL *contact) {
    if(!dst || !contact) return;
    if(dst->m_Cnt == dst->m_Capacity) {
        if(dst->m_Capacity == 0){
            dst->m_Capacity = 1;
            dst->m_Contacts = (TCRIMINAL **) malloc(dst->m_Capacity * sizeof(TCRIMINAL *));
        } else{
            dst->m_Capacity = dst->m_Capacity * 2;
            dst->m_Contacts = (TCRIMINAL **) realloc(dst->m_Contacts, dst->m_Capacity * sizeof(TCRIMINAL *));
        }
    }
    dst->m_Contacts[(dst->m_Cnt)++] = contact;
}

void freeList(TCRIMINAL *src) {
    while (src) {
        TCRIMINAL *next = src->m_Next;
        free(src->m_Name);
        src->m_Name = nullptr;
        free(src->m_Contacts);
        src->m_Contacts = nullptr;
        free(src);
        src = next;
    }
}

#ifndef __PROGTEST__

int main(int argc, char *argv[]) {
    TCRIMINAL *a, *b;
    char tmp[100];

    assert (sizeof(TCRIMINAL) == 3 * sizeof(void *) + 2 * sizeof(size_t));
    a = nullptr;
    a = createRecord("Peter", a);
    a = createRecord("John", a);
    a = createRecord("Joe", a);
    a = createRecord("Maria", a);
    addContact(a, a->m_Next);
    addContact(a->m_Next->m_Next, a->m_Next->m_Next->m_Next);
    addContact(a->m_Next->m_Next->m_Next, a->m_Next);
    assert (a
            && !strcmp(a->m_Name, "Maria")
            && a->m_Cnt == 1
            && a->m_Contacts[0] == a->m_Next);
    assert (a->m_Next
            && !strcmp(a->m_Next->m_Name, "Joe")
            && a->m_Next->m_Cnt == 0);
    assert (a->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Name, "John")
            && a->m_Next->m_Next->m_Cnt == 1
            && a->m_Next->m_Next->m_Contacts[0] == a->m_Next->m_Next->m_Next);
    assert (a->m_Next->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Next->m_Name, "Peter")
            && a->m_Next->m_Next->m_Next->m_Cnt == 1
            && a->m_Next->m_Next->m_Next->m_Contacts[0] == a->m_Next);
    assert (a->m_Next->m_Next->m_Next->m_Next == nullptr);
    b = cloneList(a);
    strcpy(tmp, "Moe");
    a = createRecord(tmp, a);
    strcpy(tmp, "Victoria");
    a = createRecord(tmp, a);
    strcpy(tmp, "Peter");
    a = createRecord(tmp, a);
    addContact(b->m_Next->m_Next->m_Next, b->m_Next->m_Next);
    assert (a
            && !strcmp(a->m_Name, "Peter")
            && a->m_Cnt == 0);
    assert (a->m_Next
            && !strcmp(a->m_Next->m_Name, "Victoria")
            && a->m_Next->m_Cnt == 0);
    assert (a->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Name, "Moe")
            && a->m_Next->m_Next->m_Cnt == 0);
    assert (a->m_Next->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Next->m_Name, "Maria")
            && a->m_Next->m_Next->m_Next->m_Cnt == 1
            && a->m_Next->m_Next->m_Next->m_Contacts[0] == a->m_Next->m_Next->m_Next->m_Next);
    assert (a->m_Next->m_Next->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Next->m_Next->m_Name, "Joe")
            && a->m_Next->m_Next->m_Next->m_Next->m_Cnt == 0);
    assert (a->m_Next->m_Next->m_Next->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Name, "John")
            && a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Cnt == 1
            && a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Contacts[0] ==
               a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Next);
    assert (a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Next->m_Name, "Peter")
            && a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Next->m_Cnt == 1
            && a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Next->m_Contacts[0] == a->m_Next->m_Next->m_Next->m_Next);
    assert (a->m_Next->m_Next->m_Next->m_Next->m_Next->m_Next->m_Next == nullptr);
    assert (b
            && !strcmp(b->m_Name, "Maria")
            && b->m_Cnt == 1
            && b->m_Contacts[0] == b->m_Next);
    assert (b->m_Next
            && !strcmp(b->m_Next->m_Name, "Joe")
            && b->m_Next->m_Cnt == 0);
    assert (b->m_Next->m_Next
            && !strcmp(b->m_Next->m_Next->m_Name, "John")
            && b->m_Next->m_Next->m_Cnt == 1
            && b->m_Next->m_Next->m_Contacts[0] == b->m_Next->m_Next->m_Next);
    assert (b->m_Next->m_Next->m_Next
            && !strcmp(b->m_Next->m_Next->m_Next->m_Name, "Peter")
            && b->m_Next->m_Next->m_Next->m_Cnt == 2
            && b->m_Next->m_Next->m_Next->m_Contacts[0] == b->m_Next
            && b->m_Next->m_Next->m_Next->m_Contacts[1] == b->m_Next->m_Next);
    assert (b->m_Next->m_Next->m_Next->m_Next == nullptr);
    freeList(a);
    addContact(b->m_Next, b->m_Next);
    a = cloneList(b);
    assert (a
            && !strcmp(a->m_Name, "Maria")
            && a->m_Cnt == 1
            && a->m_Contacts[0] == a->m_Next);
    assert (a->m_Next
            && !strcmp(a->m_Next->m_Name, "Joe")
            && a->m_Next->m_Cnt == 1
            && a->m_Next->m_Contacts[0] == a->m_Next);
    assert (a->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Name, "John")
            && a->m_Next->m_Next->m_Cnt == 1
            && a->m_Next->m_Next->m_Contacts[0] == a->m_Next->m_Next->m_Next);
    assert (a->m_Next->m_Next->m_Next
            && !strcmp(a->m_Next->m_Next->m_Next->m_Name, "Peter")
            && a->m_Next->m_Next->m_Next->m_Cnt == 2
            && a->m_Next->m_Next->m_Next->m_Contacts[0] == a->m_Next
            && a->m_Next->m_Next->m_Next->m_Contacts[1] == a->m_Next->m_Next);
    assert (a->m_Next->m_Next->m_Next->m_Next == nullptr);
    assert (b
            && !strcmp(b->m_Name, "Maria")
            && b->m_Cnt == 1
            && b->m_Contacts[0] == b->m_Next);
    assert (b->m_Next
            && !strcmp(b->m_Next->m_Name, "Joe")
            && b->m_Next->m_Cnt == 1
            && b->m_Next->m_Contacts[0] == b->m_Next);
    assert (b->m_Next->m_Next
            && !strcmp(b->m_Next->m_Next->m_Name, "John")
            && b->m_Next->m_Next->m_Cnt == 1
            && b->m_Next->m_Next->m_Contacts[0] == b->m_Next->m_Next->m_Next);
    assert (b->m_Next->m_Next->m_Next
            && !strcmp(b->m_Next->m_Next->m_Next->m_Name, "Peter")
            && b->m_Next->m_Next->m_Next->m_Cnt == 2
            && b->m_Next->m_Next->m_Next->m_Contacts[0] == b->m_Next
            && b->m_Next->m_Next->m_Next->m_Contacts[1] == b->m_Next->m_Next);
    assert (b->m_Next->m_Next->m_Next->m_Next == nullptr);
    freeList(b);
    freeList(a);
    return EXIT_SUCCESS;
}

#endif /* __PROGTEST__ */
