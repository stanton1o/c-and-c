#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define MAX_WORD_LENGTH 5001

static char WORD1[MAX_WORD_LENGTH * 2];
static char WORD2[MAX_WORD_LENGTH * 2];
void toLowerCase(char *str) {
    for (int i = 0; str[i]; i++) {
        str[i] = tolower((unsigned char) str[i]);
    }
}

//Сравнение слов как 1 целого
int cmp(const void *word1, const void *word2) {
    const char *word1x = *(const char **)word1;
    const char *word2x = *(const char **)word2;
    strcpy(WORD2, word2x);
    strcpy(WORD1, word1x);
    strcat(WORD2, word1x);
    strcat(WORD1, word2x);
    toLowerCase(WORD1);
    toLowerCase(WORD2);
    int strcmp_res = strcmp(WORD2, WORD1);
    if(strcmp_res){
        return strcmp_res;
    } else
        return (int)(strlen(word2x) - strlen(word1x));
}

//Проверка на наличие повторов и др неверных данных
int isWrongInput(char **words, int word_count){
    for (int i = 0; i < word_count - 1; i++) {
        char *tmp1 = strdup(words[i]);
        toLowerCase(tmp1);
        char *tmp2 = strdup(words[i + 1]);
        toLowerCase(tmp2);
        if(!strcmp(tmp1, tmp2)){
            free(tmp1);
            free(tmp2);
            return 1;
        }
        free(tmp1);
        free(tmp2);
    }
    return 0;
}

int main() {
    char **words = NULL;
    int words_max_size = 0, word_count = 0;
    char word[MAX_WORD_LENGTH] = {0};

    printf("Slova:\n");

    int isEmptyInput = 1;

    while (scanf(" %5000s", word) != EOF) {
        isEmptyInput = 0;
        if (word_count == words_max_size) {
            words_max_size = words_max_size * 2 + 10;
            words = (char**)realloc(words, words_max_size * sizeof(char *));
            if (!words) {
                perror("Error allocating memory");
                return 1;
            }
        }
        words[word_count] = strdup(word);
        if (!words[word_count]) {
            perror("Error allocating memory");
            return 1;
        }
        word_count++;
    }

    if (isEmptyInput) {
        printf("Nespravny vstup.\n");
        return 1;
    }else
        //Сортировка для создания наиболее сильной комбинации.
        qsort(words, word_count, sizeof(char *), cmp);

    if(isWrongInput(words, word_count)){
        printf("Nespravny vstup.\n");
        for(int i = 0; i < word_count; i++){
            free(words[i]);
        }
        free(words);
        return 1;
    }
    size_t symbolsInLine = 0;
    int currentWord = 0;
    char last_symbol;
    //Логика по печати текста.
    while(currentWord < word_count){
        printf("%s", words[currentWord]);
        if(currentWord == word_count - 1){
            last_symbol = '\n';
        } else{
            symbolsInLine += strlen(words[currentWord]);
            if(symbolsInLine + strlen(words[currentWord + 1]) > 79){
                symbolsInLine = 0;
                last_symbol = '\n';
            }
            else{
                last_symbol = ' ';
                symbolsInLine++;
            }
        }
        printf("%c", last_symbol);
        free(words[currentWord++]);
    }
    free(words);
    return 0;
}

