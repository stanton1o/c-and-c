#!/bin/bash
# Otestuje program podle referenčních dat ze souborů *_in.txt a porovná je s referenčním výstupem *_out.txt
# Soubory musejí být uloženy ve složce "CZE"

TEST_DIR="CZE/"
SUCCESS_COUNT=0
MAX_COUNT=0

for IN_FILE in ${TEST_DIR}*_in.txt
do
	REF_FILE=$(echo -n $IN_FILE | sed -e 's/_in\(.*\)$/_out\1/')

	./proga < $IN_FILE > my_out.txt
  let MAX_COUNT++
	if ! diff $REF_FILE my_out.txt > temp.txt
	then
		printf "\e[0;31mFail: %s\e[0m\n" "$IN_FILE"

		sed -e ':a;N;$!ba;s/\n/ /g' my_out.txt| sed -e 's/  */ /g' > tmp.txt
		sed -e ':a;N;$!ba;s/\n/ /g' $REF_FILE | sed -e 's/  */ /g' > tmp2.txt
    # Сравнение файлов после преобразования
    if diff tmp.txt tmp2.txt > temp2.txt
    then
        printf "\e[0;33mПроебался в белых знаках: \e[0m %s\n" $IN_FILE
    else
        printf "\e[0;31mПроебался где то еще: %s\e[0m\n" "$IN_FILE"
    fi
    #cat temp.txt
    #echo "Vstupni data:"
    #echo `cat "$IN_FILE"`
    #exit
    rm tmp.txt tmp2.txt temp2.txt temp.txt my_out.txt
	else
		printf "\e[0;32mOK:\e[0m %s\n" $IN_FILE
		let SUCCESS_COUNT++
	fi
done

printf "\nTotal successful tests: %d/%d\n" $SUCCESS_COUNT $MAX_COUNT
if [ $SUCCESS_COUNT -eq $MAX_COUNT ]
then
  printf "\e[0;32mЗаебись, чётко!\e[0m\n"
else
  printf "\e[0;31mГовно, переделывай!\e[0m\n"
fi

rm my_out.txt temp.txt
