#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <stdbool.h>
#include <time.h>

constexpr int MAP_MAX = 200;

// обозначение границ прямоугольника
typedef struct {
    int leftCol;
    int rightCol;
    int topRow;
    int bottomRow;
}t_rect;

// проверка высоты внутри
bool checkRectangle(int altitude[][MAP_MAX], t_rect rectangle, int i, int val){
    for(int j = rectangle.topRow; j <= rectangle.bottomRow; j++){
            if(altitude[j][i] >= val)
                return false;
    }
    return true;
}

//постепенное расширение во все стороны для нахождения макисмально возможного прямоугольника из стартовых кооридант
void findMaxRectangle(int altitude[][MAP_MAX], int size, t_rect* maxRectangle, int val, int col, int row){
    for(int i = col + 1 ; i < size ; ++i){
        if(altitude[row][i] >= val){
            break;
        }
        maxRectangle->rightCol = i;
    }
    for(int i = col - 1; i >= 0 ; --i){
        if(altitude[row][i] >= val){
            break;
        }
        maxRectangle->leftCol = i;
    }
    for(int j = row + 1 ; j < size ; ++j){
        if(altitude[j][col] >= val){
            break;
        }
        maxRectangle->bottomRow = j;
    }
    for(int j = row - 1; j >= 0 ; --j){
        if(altitude[j][col] >= val){
            break;
        }
        maxRectangle->topRow = j;
    }
}
void castleArea(int altitude[][MAP_MAX], int size, int area[][MAP_MAX]) {
    for(int row = 0; row < size; row++){
        for(int col = 0; col < size; col++){

            t_rect maxRectangle = {col, col, row, row};
            int valueOnPosition = altitude[row][col];
            findMaxRectangle(altitude, size, &maxRectangle, valueOnPosition, col, row);

            area[row][col] = 1;
            //move the top border down
            for(int top = maxRectangle.topRow; top <= row; top++){
                //move the bottom border up
                for(int bottom = maxRectangle.bottomRow; bottom >= row ; bottom--){

                    int L = col, R = col;
                    t_rect currentRectangle = {-1, -1, top, bottom};

                    //move the left border to the left as far as we can
                    for(int left = col - 1; left >= maxRectangle.leftCol; left--){
                        if(!checkRectangle(altitude, currentRectangle, left, valueOnPosition))
                            break;
                        L = left;
                    }
                    //move the right border to the right as far as we can
                    for(int right = col + 1; right <= maxRectangle.rightCol; right++){
                        if(!checkRectangle(altitude, currentRectangle, right, valueOnPosition))
                            break;
                        R = right;
                    }

                    int sizeRect = (R - L + 1) * (bottom - top + 1);
                    if(area[row][col] < sizeRect)
                        area[row][col] = sizeRect;
                }
            }
        }

    }

}

bool identicalMap ( const int a[][MAP_MAX], const int b[][MAP_MAX], int n )
{
    for(int i = 0 ; i < n ; ++i){
        for(int j = 0 ; j < n ; ++j){
            if(a[i][j] != b[i][j])
                return false;
        }
    }
    return true;
}
int main ( int argc, char * argv [] )
{

    static int result[MAP_MAX][MAP_MAX];

    static int alt0[MAP_MAX][MAP_MAX] =
            {
                    { 1, 2 },
                    { 3, 4 }
            };
    static int area0[MAP_MAX][MAP_MAX] =
            {
                    { 1, 2 },
                    { 2, 4 }
            };


    castleArea ( alt0, 2, result );
    assert ( identicalMap ( result, area0, 2 ) );
    static int alt1[MAP_MAX][MAP_MAX] =
            {
                    { 2, 7, 1, 9 },
                    { 3, 5, 0, 2 },
                    { 1, 6, 3, 5 },
                    { 1, 2, 2, 8 }
            };
    static int area1[MAP_MAX][MAP_MAX] =
            {
                    { 1, 12, 2, 16 },
                    { 4, 4, 1, 2 },
                    { 1, 9, 4, 4 },
                    { 1, 2, 1, 12 }
            };
    castleArea ( alt1, 4, result );

    assert ( identicalMap ( result, area1, 4 ) );
    static int alt2[MAP_MAX][MAP_MAX] =
            {
                    { 1, 2, 3, 4 },
                    { 2, 3, 4, 5 },
                    { 3, 4, 5, 6 },
                    { 4, 5, 6, 7 }
            };
    static int area2[MAP_MAX][MAP_MAX] =
            {
                    { 1, 2, 3, 4 },
                    { 2, 4, 6, 8 },
                    { 3, 6, 9, 12 },
                    { 4, 8, 12, 16 }
            };
    castleArea ( alt2, 4, result );
    assert ( identicalMap ( result, area2, 4 ) );
    static int alt3[MAP_MAX][MAP_MAX] =
            {
                    { 7, 6, 5, 4 },
                    { 6, 5, 4, 5 },
                    { 5, 4, 5, 6 },
                    { 4, 5, 6, 7 }
            };
    static int area3[MAP_MAX][MAP_MAX] =
            {
                    { 12, 6, 2, 1 },
                    { 6, 2, 1, 2 },
                    { 2, 1, 2, 6 },
                    { 1, 2, 6, 12 }
            };
    castleArea ( alt3, 4, result );
    assert ( identicalMap ( result, area3, 4 ) );
    static int alt4[MAP_MAX][MAP_MAX] =
            {
                    { 1, 1, 1, 1, 1 },
                    { 1, 1, 1, 1, 1 },
                    { 1, 1, 2, 1, 1 },
                    { 1, 1, 1, 1, 1 },
                    { 1, 1, 1, 1, 1 }
            };
    static int area4[MAP_MAX][MAP_MAX] =
            {
                    { 1, 1, 1, 1, 1 },
                    { 1, 1, 1, 1, 1 },
                    { 1, 1, 25, 1, 1 },
                    { 1, 1, 1, 1, 1 },
                    { 1, 1, 1, 1, 1 }
            };
    castleArea ( alt4, 5, result );
    assert ( identicalMap ( result, area4, 5 ) );

    return EXIT_SUCCESS;
}

