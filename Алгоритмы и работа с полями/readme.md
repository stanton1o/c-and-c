Задача - реализовать функцию (не всю программу, а только функцию), которая будет вычислять размер территории, которую можно контролировать с помощью удачно расположенного замка.

Мы предполагаем, что карта имеет форму квадрата. Карта разделена на n x n квадратов, для каждого квадрата известно его возвышение. Мы предполагаем, что квадраты горизонтальны, изменение высоты происходит скачками на их границе. На любой клетке можно поставить замок, он всегда занимает целую клетку. Замок контролирует прямоугольную территорию, для контролируемой территории должно выполняться следующее:

-замок находится внутри прямоугольника контролируемой территории,
-замок расположен на самой высокой точке контролируемой территории, то есть все остальные клетки на контролируемой территории имеют меньшую высоту,
-замок старается контролировать как можно больше территории.
-Задача состоит в том, чтобы реализовать функцию castleArea. Эта функция получает параметр карты с высотами (двумерный массив) и размерность карты. Для каждого квадрата карты функция вычисляет, какую территорию будет контролировать замок, расположенный в этом месте.


void castleArea ( int altitude[][MAP_MAX], int size, int area[][MAP_MAX] ) - Функция для вычисления размера контролируемой области. Параметры:

Входной параметр altitude - двумерный массив с заполненными высотами. Массив имеет заполненные элементы от [0][0] до [size-1][size-1]. Высоты доступны только для чтения, функция не должна изменять содержимое поля.
size - размер обрабатываемой карты. Карта имеет форму квадрата size x size.
Выходной параметр area, функция поместит в элемент area[y][x] максимальный размер области, которая будет контролироваться замком в позиции [y][x]. Функция заполнит только элементы [0][0] - [size-1][size-1].
bool identicalMap ( int a[][MAP_MAX], int b[][MAP_MAX], int size ) - Функция для сравнения содержимого двух карт.Функция сравнивает двумерные массивы a и b.
Сравнивается только вырезка этих массивов [0][0] - [size-1][size-1]. Функция возвращает true, если массивы имеют одинаковое содержимое в этой вырезке, или false, если содержимое вырезок разное.
